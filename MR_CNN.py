import tensorflow as tf
import numpy as np
import random
import data_helpers
import collections
import tokenization
from tensorflow.contrib import learn
from embedding import Embedding as Em

tf.logging.set_verbosity(tf.logging.INFO)

conf = collections.namedtuple("conf", "pos, neg, sen_len, test_sam, voc, batch_size, emb_size,"
                              "fil_num, dro_rate, class_num, do_lower_case, init_checkpoint, vocab_file")

conf.pos = "./GLUE_DIR/glue_data/MR/rt-polarity.pos"
conf.neg = "./GLUE_DIR/glue_data/MR/rt-polarity.neg"
conf.sen_len = 82
conf.test_sam = 0.1
conf.voc = 18758
conf.batch_size = 32
conf.emb_size = 768
conf.fil_num = 128
conf.dro_rate = 0.5
conf.class_num = 2
conf.do_lower_case = False
conf.init_checkpoint = "./BERT_BASE_DIR/cased_L-12_H-768_A-12/bert_model.ckpt"
conf.vocab_file = "./BERT_BASE_DIR/cased_L-12_H-768_A-12/vocab.txt"


def batch_generator(X, y, batch_size=32, shuffle=False, random_seed=None):
    idx = np.arange(y.shape[0])
    if shuffle:
        rng = np.random.RandomState(random_seed)
        rng.shuffle(idx)
        X = X[idx]
        y = y[idx]
    for i in range(0, X.shape[0], batch_size):
        yield (X[i: i + batch_size, :], y[i: i + batch_size])


# def file_based_convert_examples_to_features(
#     examples, label_list, max_seq_length, tokenizer):
#   """Convert a set of `InputExample`s to a TFRecord file."""
#
#   for (ex_index, example) in enumerate(examples):
#     if ex_index % 10000 == 0:
#       tf.logging.info("Writing example %d of %d" % (ex_index, len(examples)))
#
#     feature = convert_single_example(ex_index, example, label_list,
#                                      max_seq_length, tokenizer)
#
#     def create_int_feature(values):
#       f = tf.train.Feature(int64_list=tf.train.Int64List(value=list(values)))
#       return f
#
#     features = collections.OrderedDict()
#     features["input_ids"] = create_int_feature(feature.input_ids)
#     features["input_mask"] = create_int_feature(feature.input_mask)
#     features["segment_ids"] = create_int_feature(feature.segment_ids)
#     features["label_ids"] = create_int_feature([feature.label_id])
#     features["is_real_example"] = create_int_feature(
#         [int(feature.is_real_example)])
#
#     tf_example = tf.train.Example(features=tf.train.Features(feature=features))



def build_cnn():
    # placeholders for X and y:
    tf_x = tf.placeholder(tf.int32, shape=[None, conf.sen_len], name='tf_x')
    tf_y = tf.placeholder(tf.int32, shape=[None], name='tf_y')
    tf_y_onehot = tf.one_hot(indices=tf_y, depth=2, dtype=tf.float32)
    keep_prob = tf.placeholder(tf.float32, name='fc_keep_prob')

    # W = tf.Variable(tf.random_uniform([conf.voc, conf.emb_size], -1.0, 1.0), name="W")
    # embedded_chars = tf.nn.embedding_lookup(W, tf_x, None)    #embedded_chars : 64  *  56*128
    # reshape x to a 4D tensor: [batchsize, width, height, 1]
    # print('tf_x', tf_x)
    embedded_chars = Em(tf_x, max_seq_length=conf.sen_len, batch_size=32)
    print('embedded_chars', embedded_chars)
    embedded_chars_expanded = tf.expand_dims(embedded_chars, -1)  # embedded_chars_expanded : 64  *  56*128 * 1

    pooled_outputs = []

    #CNN
    #filter size : 3、4、5
    for i in range(3, 6):
        # convolutional layer
        conv = tf.layers.conv2d(
             inputs=embedded_chars_expanded,
             filters=conf.fil_num,
             kernel_size=[i, conf.emb_size],
             padding="VALID",
             activation=tf.nn.relu)   #conv : (56 - i + 1)*1

        #max pooling
        pools = tf.layers.max_pooling2d(inputs=conv, pool_size=[conf.sen_len - i + 1, 1], strides=1)   #pool : 1*1
        pool_outputs1 = tf.reshape(pools, [-1, conf.fil_num])   #pool_outputs1 : 64  *  128

        #記錄不同的filter size做完後的結果
        pooled_outputs.append(pool_outputs1)           #pooled_outputs.append : 64  *  3*128

    #將不同的filter size做完後的結果整合
    pool2 = tf.concat(pooled_outputs, 1)             #pool2 : 64  *  384

    #dropout
    dropout = tf.nn.dropout(pool2, keep_prob=keep_prob, name='dropout_layer')
    # dropout = tf.layers.dropout(
    #   inputs=pool2, rate=conf.dro_rate, training=mode == tf.estimator.ModeKeys.TRAIN)   #dropout : 64  *  384

    #fully connected layer
    W1 = tf.get_variable("W",
                         shape=[3*conf.fil_num, conf.class_num],
                         initializer=tf.contrib.layers.xavier_initializer())  # W1 : 384*2
    b1 = tf.Variable(tf.constant(0.0, shape=[conf.class_num]), name="b")  # b1 : 2*1
    scores = tf.nn.xw_plus_b(dropout, W1, b1, name="scores")  # scores : 64  *  2*1

    predictions = {'labels': tf.cast(tf.argmax(input=scores, axis=1), tf.int32, name='labels'),
                   "probabilities": tf.nn.softmax(scores, name="softmax_tensor")}

    # Loss Function and Optimization
    cross_entropy_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=scores, labels=tf_y_onehot),
                                        name='cross_entropy_loss')

    # Optimizer:
    optimizer = tf.train.AdamOptimizer(learning_rate=1e-4)
    optimizer = optimizer.minimize(cross_entropy_loss, name='train_op')

    # Computing the prediction accuracy
    correct_predictions = tf.equal(predictions['labels'], tf_y, name='correct_preds')
    accuracy = tf.reduce_mean(tf.cast(correct_predictions, tf.float32), name='accuracy')


def train(sess, validation_set=None, initialize=True, epochs=5, shuffle=True, dropout=0.5, random_seed=None):
    tokenization.validate_case_matches_checkpoint(conf.do_lower_case, conf.init_checkpoint)
    tokenizer = tokenization.FullTokenizer(vocab_file=conf.vocab_file, do_lower_case=conf.do_lower_case)

    x_data, y_data = data_helpers.load_data_and_labels(conf.pos, conf.neg)
    # vocab_processor = learn.preprocessing.VocabularyProcessor(max_document_length)
    # x = np.array(list(vocab_processor.fit_transform(x_data)))
    X_train = np.concatenate([x_data[:4800], x_data[5331: 5331 + 4800]], axis=0)
    X_test = np.concatenate([x_data[4800:5331], x_data[5331 + 4800:]], axis=0)
    train_data = []
    test_data = []
    for i in X_train:
        kk = tokenization.convert_to_unicode(i)
        kk = tokenizer.tokenize(kk)
        kk = tokenizer.convert_tokens_to_ids(kk)
        zero = np.zeros(shape=[conf.sen_len - len(kk)], dtype=np.int32)
        kk = np.concatenate([np.array(kk), zero], axis=0)
        train_data.append(kk)
    for i in X_test:
        kk = tokenization.convert_to_unicode(i)
        kk = tokenizer.tokenize(kk)
        kk = tokenizer.convert_tokens_to_ids(kk)
        zero = np.zeros(shape=[conf.sen_len - len(kk)], dtype=np.int32)
        kk = np.concatenate([np.array(kk), zero], axis=0)
        test_data.append(kk)
    # tokenizer.convert_tokens_to_ids
    # aa = tokenizer.tokenize(train_data)
    X_train = np.array(train_data)
    X_test = np.array(test_data)
    y_train = np.expand_dims(np.concatenate([y_data[:4800], y_data[5331: 5331 + 4800]], axis=0), -1)
    y_test = np.expand_dims(np.concatenate([y_data[4800:5331], y_data[5331 + 4800:]], axis=0), -1)

    train_data = np.concatenate([X_train, y_train], axis=1)
    test_data = np.concatenate([X_test, y_test], axis=1)
    random.shuffle(train_data)
    random.shuffle(test_data)

    training_loss = []
    # initialize variables
    if initialize:
        sess.run(tf.global_variables_initializer())
    np.random.seed(random_seed)  # for shuflling in batch generator
    for epoch in range(1, epochs + 1):
        avg_loss = 0.0
        train_acc = []
        for i in range(int(y_train.shape[0] / 32)):
            x_train = train_data[i*32: (i+1)*32, 0:conf.sen_len]
            y_train = train_data[i*32: (i+1)*32, conf.sen_len]
            if len(y_train) < 32:
                break
            feed = {'tf_x:0': x_train,
                    'tf_y:0': y_train,
                    'fc_keep_prob:0': dropout}
            loss, _, acc = sess.run(['cross_entropy_loss:0', 'train_op', 'accuracy:0'], feed_dict=feed)
            avg_loss += loss
            train_acc.append(acc)
            training_loss.append(avg_loss / (i + 1))
        print('Epoch %02d Training Avg. Loss: %7.5f' % (epoch, avg_loss), 'train acc', np.mean(train_acc))
        if epoch % 5 == 0:
            dev_acc = []
            for i in range(int(len(y_test) / 32)):
                feed = {'tf_x:0': test_data[32*i:32*(i+1), 0:conf.sen_len],
                        'tf_y:0': test_data[32*i:32*(i+1), conf.sen_len],
                        'fc_keep_prob:0': 1.0}
                valid_acc, pred = sess.run(['accuracy:0', 'labels:0'], feed_dict=feed)
                dev_acc.append(valid_acc)
            print('===========================================')
            print(' Validation Acc: %7.3f' % np.mean(dev_acc))
            print('===========================================')


# Define hyperparameters
learning_rate = 1e-4
random_seed = 123
# create a graph
g = tf.Graph()
with g.as_default():
    tf.set_random_seed(random_seed)
    # build the graph
    build_cnn()

# create a TF session and train the CNN model
with tf.Session(graph=g) as sess:
    train(sess, initialize=True, random_seed=123)

# Model
# def model_fn_1(features, labels, mode):
#
#   # Input Layer
#   input_layer = tf.reshape(features["x"], [-1, conf.sen_len])   #input_layer : 64  *  56
#
#   #word embedding
#   W = tf.Variable(
#       tf.random_uniform([conf.voc, conf.emb_size], -1.0, 1.0),     #W : 18758*128
#       name="W")
#   embedded_chars = tf.nn.embedding_lookup(W, input_layer, None)    #embedded_chars : 64  *  56*128
#   # embedded_chars = Em(input_layer, max_seq_length=56, batch_size=64)
#   embedded_chars_expanded = tf.expand_dims(embedded_chars, -1)     #embedded_chars_expanded : 64  *  56*128 * 1
#
#   pooled_outputs = []
#
#   #CNN
#   #filter size : 3、4、5
#   for i in range(3, 6):
#     #convolutional layer
#     conv = tf.layers.conv2d(
#          inputs=embedded_chars_expanded,
#          filters=conf.fil_num,
#          kernel_size=[i, conf.emb_size],
#          padding="VALID",
#          activation=tf.nn.relu)   #conv : (56 - i + 1)*1
#
#     #max pooling
#     pools = tf.layers.max_pooling2d(inputs=conv, pool_size=[conf.sen_len - i + 1, 1], strides=1)   #pool : 1*1
#     pool_outputs1 = tf.reshape(pools, [-1, conf.fil_num])   #pool_outputs1 : 64  *  128
#
#     #記錄不同的filter size做完後的結果
#     pooled_outputs.append(pool_outputs1)           #pooled_outputs.append : 64  *  3*128
#
#   #將不同的filter size做完後的結果整合
#   pool2 = tf.concat(pooled_outputs, 1)             #pool2 : 64  *  384
#
#   #dropout
#   dropout = tf.layers.dropout(
#       inputs=pool2, rate=conf.dro_rate, training=mode == tf.estimator.ModeKeys.TRAIN)   #dropout : 64  *  384
#
#   #fully connected layer
#   W1 = tf.get_variable(
#       "W",
#       shape=[3*conf.fil_num, conf.class_num],
#       initializer=tf.contrib.layers.xavier_initializer())                  #W1 : 384*2
#   b1 = tf.Variable(tf.constant(0.1, shape = [conf.class_num]), name="b")   #b1 : 2*1
#   scores = tf.nn.xw_plus_b(dropout, W1, b1, name="scores")                  #scores : 64  *  2*1
#
#
#   #predictions
#   predictions = {
#       # Generate predictions (for PREDICT and EVAL mode)
#       "classes": tf.argmax(input=scores, axis=1),
#       # Add `softmax_tensor` to the graph. It is used for PREDICT and by the `logging_hook`.
#       "probabilities": tf.nn.softmax(scores, name="softmax_tensor")
#   }
#
#   if mode == tf.estimator.ModeKeys.PREDICT:
#     return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)
#
#   # Calculate Loss (for both TRAIN and EVAL modes)
#   loss = tf.losses.softmax_cross_entropy(onehot_labels=tf.one_hot(indices=labels, depth=2, dtype=tf.float32),
#                                          logits=scores)
#
#
#   # Configure the Training Op (for TRAIN mode)
#   if mode == tf.estimator.ModeKeys.TRAIN:
#       optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.1)
#       train_op = optimizer.minimize(
#            loss=loss,
#            global_step=tf.train.get_global_step())
#       #optimizer = tf.train.AdamOptimizer(0.001)
#       #train_op = optimizer.minimize(loss, global_step=tf.train.get_global_step())
#       return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)
#
#   # Add evaluation metrics (for EVAL mode)
#   eval_metric_ops = {
#       "accuracy": tf.metrics.accuracy(
#           labels=tf.argmax(input=labels, axis=1), predictions=predictions["classes"])}
#   return tf.estimator.EstimatorSpec(
#       mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)
#
# def main(unused_argv):
#
#     #data preprocess
#     x_data, y = data_helpers.load_data_and_labels(conf.pos, conf.neg)
#
#     # Build vocabulary
#     max_document_length = max([len(x.split(" ")) for x in x_data])
#     vocab_processor = learn.preprocessing.VocabularyProcessor(max_document_length)
#     x = np.array(list(vocab_processor.fit_transform(x_data)))
#
#     # Randomly shuffle data
#     np.random.seed(10)
#     shuffle_indices = np.random.permutation(np.arange(len(y)))
#     x_shuffled = x[shuffle_indices]
#     y_shuffled = y[shuffle_indices]
#
#     # Split train/test set
#     test_sample_index = -1 * int(conf.test_sam * float(len(y)))
#     x_train, x_test = x_shuffled[:test_sample_index], x_shuffled[test_sample_index:]
#     y_train, y_test = y_shuffled[:test_sample_index], y_shuffled[test_sample_index:]
#
#     x_train1 = np.asarray(x_train)
#     y_train1 = np.asarray(y_train, dtype=np.int32)
#
#     x_test1 = np.asarray(x_test)
#     y_test1 = np.asarray(y_test, dtype=np.int32)
#
#     del x, y, x_shuffled, y_shuffled
#
#     print("Vocabulary Size: {:d}".format(len(vocab_processor.vocabulary_)))
#     print("Train/Test split: {:d}/{:d}".format(len(y_train), len(y_test)))
#
#
#     # Create the Estimator
#     text_classifier = tf.estimator.Estimator(model_fn=model_fn_1, model_dir="./models")
#
#     # Set up logging for predictions
#     tensors_to_log = {"probabilities": "softmax_tensor"}
#
#     # Train the model
#     train_input_fn = tf.estimator.inputs.numpy_input_fn(
#       x={"x": x_train1},
#       y=y_train1,
#       batch_size=conf.batch_size,
#       num_epochs=None,
#       shuffle=True)
#     text_classifier.train(
#       input_fn=train_input_fn,
#       steps=1000)
#
#     # Evaluate the model and print results
#     eval_input_fn = tf.estimator.inputs.numpy_input_fn(
#       x={"x": x_test1},
#       y=y_test1,
#       num_epochs=1,
#       shuffle=False)
#     eval_results = text_classifier.evaluate(input_fn=eval_input_fn)
#     print(eval_results)
#
#
# if __name__ == "__main__":
#   tf.app.run()